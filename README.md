# TP Sockets

L'objectif de ce TP est de créer un programme client-serveur. Le client et le serveur sont 
deux programmes distincts qui peuvent fonctionner sur des machines différentes, ou sur la 
même machine.


Pour compiler le programme :
gcc -o serveur serveur.c
gcc -o client client.c

Ensuite exécuter d'abord le programme serveur en tapant:
./serveur

Ensuite exécuter dans un autre terminal le programme client en tapant :
./client

Puis sur le terminal où s'exécute le programme client, suivez les indications du menu.

NB : Il est important que les deux programmes soient exécutés dans deux terminaux différents
     Pensez aussi à exécuter le programme serveur avant le programme client.
