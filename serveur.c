/*TP Réseau sur les Sockets : réalisé par Adama NANA*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
//IPV4
//TCP 
typedef struct ClientToServeur {
    char phrase[100];
    int code;
}ClientToServeur;

//gcc -o serveur serveur.c

int longueurPhrase(char phrase[]) {
    int i=0;
    while(phrase[i] != '\0') {
        i++;
    }
    return i;
}

char *triePhrase(char phrase[]) {
    int taille = longueurPhrase(phrase);
    for(int i=0; i < taille; i++) {
        int min = i;
        for(int j=i+1; j< taille; j++) {
            if(phrase[min] > phrase[j]) {
                char tmp = phrase[min];
                phrase[min] = phrase[j];
                phrase[j] = tmp;
            }
        }
    }
    return phrase;
}

int main (int argc, char ** argv) {

    int socketServer = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in addrServer;
    addrServer.sin_addr.s_addr = INADDR_ANY;
    addrServer.sin_family = AF_INET;
    addrServer.sin_port = htons(30000);

    bind(socketServer, (const struct sockaddr *)&addrServer, sizeof(addrServer));
    printf("bind : %d\n", socketServer);

    //Peut avoir jusqu'à 5 connexions en attente
    listen(socketServer, 5);
    printf("Listenning\n");

    struct sockaddr_in addrClient;
    socklen_t csize = sizeof(addrClient);
    //Là il va y avoir une boucle infinie et le serveur attendre qu'un client se connecte
    int socketClient = accept(socketServer, (struct sockaddr *)&addrClient, &csize);
    printf("Accept");
    printf("Client : %d\n", socketClient);

    ClientToServeur cts;
    cts.code = 10;
    int working = 1;
    int cmp=0;
    while (working) {
        char stringBuilder[100];
        recv(socketClient, &cts, sizeof(cts), 0);
        strcpy(stringBuilder, cts.phrase);
        if(cts.code == 1) {
            printf("Moi le serveur j'ai reçu la phrase : %s %d\n", cts.phrase, cmp);
            int longueur = longueurPhrase(cts.phrase);
            send(socketClient, &longueur, sizeof(longueur), 0);
        } else if(cts.code == 2) {
            printf("Moi le serveur j'ai reçu la phrase : %s %d\n", cts.phrase, cmp);
            char phraseTriee[100];
            strcpy(phraseTriee, triePhrase(cts.phrase));
            send(socketClient, &phraseTriee, sizeof(phraseTriee), 0);
        } else {
            working = 0;
        }
        cts.code = 0;
        cts.phrase[0] = '\0';
        cmp++;
    }
    close(socketClient);
    close(socketServer);
    printf("Client déconnecté\n");
    return 0;
}