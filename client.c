/*TP Réseau sur les Sockets : réalisé par Adama NANA*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>

#define STR(s) #s
#define XSTR(s) STR(s)
#define SIZE 200

void clean_stdin(void) {
    int c = 0;
    while ((c = getchar()) != '\n' && c != EOF);
}

typedef struct ClientToServeur {
    char phrase[100];
    int code;
}ClientToServeur;


int main (int argc, char ** argv) {

    int socketClient = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in addrClient;
    addrClient.sin_addr.s_addr = INADDR_ANY;
    addrClient.sin_family = AF_INET;
    addrClient.sin_port = htons(30000);

    connect(socketClient, (const struct sockaddr *)&addrClient, sizeof(addrClient));
    printf("Début de connexion\n");
    printf("Connecté\n");

    int working = 1;
    while(working) {
        printf("========================= MENU =========================\n");
        printf("Tapez 1 si vous voulez la longueur d'une phrase\n");
        printf("Tapez 2 si vous voulez l'ordre des lettres d'une phrase\n");
        printf("Tapez 0 pour quitter le programme\n");
        int choix=0;
        choix = fgetc(stdin);
        if(choix == '0') {
            printf("L'utilisateur a choisi de quitter le programme\n");
            working = 0;
        } else {
            if(choix != '1' && choix != '2') {
                printf("L'utilisateur n'a fait aucun choix cohérent.\n");
            } else {
                ClientToServeur cts;
                clean_stdin();
                char *phrase = malloc(100*sizeof(char));
                printf("Saisissez la phrase\n");
                scanf("%"XSTR(SIZE)"[^\n]", phrase);
                clean_stdin();
                strcpy(cts.phrase, phrase);
                if(choix == '1') {
                    cts.code = 1;
                    printf("L'utilisateur a choisi de connaitre la longueur de la phrase : %s.\n", cts.phrase);
                    int longueur;
                    send(socketClient, &cts, sizeof(cts), 0);
                    recv(socketClient, &longueur, sizeof(longueur), 0);
                    printf("La phrase compte %d carractères.\n", longueur);
                } else if(choix == '2') {
                    cts.code = 2;
                    char phraseTriee[100];
                    printf("L'utilisateur choisi de trier les caractères de la phrase : %s.\n", cts.phrase);
                    send(socketClient, &cts, sizeof(cts), 0);
                    recv(socketClient, &phraseTriee, sizeof(phraseTriee), 0);
                    printf("Résultat du trie : %s.\n", phraseTriee);   
                } 
            }           
        }
    }

    close(socketClient);
    printf("Fin de connexion\n");
    return 0;
}